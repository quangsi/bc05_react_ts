import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DemoProps from "./DemoProps/DemoProps";
import Ex_Todo_List from "./Ex_Todo_List/Ex_Todo_List";

function App() {
  return (
    <div className="App">
      {/* <DemoProps /> */}
      <Ex_Todo_List />
    </div>
  );
}

export default App;
