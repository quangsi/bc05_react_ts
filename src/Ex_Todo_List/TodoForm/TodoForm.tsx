import React, { useState } from "react";
import {
  InterfaceTodo,
  InterfaceTodoFormComponent,
} from "../Interface/interface_Ex_TodoList";
import { nanoid } from "nanoid";
type Props = {};

export default function TodoForm({
  handleAddTodo,
}: InterfaceTodoFormComponent) {
  const [title, setTitle] = useState<string>("");

  let handleOnchangTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  };

  let handleSubmit = () => {
    let objectTodo: InterfaceTodo = {
      id: nanoid(5),
      title: title,
      isCompleted: false,
    };
    handleAddTodo(objectTodo);
  };
  return (
    <div>
      <div className="form-group">
        <input
          value={title}
          onChange={handleOnchangTitle}
          type="text"
          className="form-control"
          placeholder="Title"
        />
      </div>
      <button onClick={handleSubmit} className="btn btn-warning">
        Add Todo
      </button>
    </div>
  );
}
